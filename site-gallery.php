<?php
/*
Template Name: Gallery
 */
include("parts/html-header.php");
?>
<body class="gallery">
<?php include("parts/header.php"); ?>
<section class="stopper spacing spacing-top">
    <!-- Flexslider Start -->
    <?php if(have_rows('slider')): ?>
      <div class="flexslider flexslider-standard">
        <ul class="slides">
          <?php while(have_rows('slider')): the_row(); ?>
            <li>
                <?php
                  $attachment_id = get_sub_field('image');
                  $size = "slider-normal-image"; // (thumbnail, medium, large, full or custom size)
                  $image = wp_get_attachment_image_src( $attachment_id, $size);
                ?>
                <img src="<?php echo $image[0]; ?>" alt="Gallery Image">
                <?php if( get_sub_field( "text" ) ): ?>
                  <div class="infotext">
                    <p><?php the_sub_field("text"); ?></p>
                    <?php if( get_sub_field( "link" ) ): ?>
                      <a class="button" href="<?php the_sub_field("link"); ?>"><?php the_sub_field("link_text"); ?></a>
                    <?php endif; ?>
                  </div>
                <?php endif; ?>
            </li>
          <?php endwhile; ?>
        </ul>
      </div>
    <?php endif; ?>
    <!-- Flexslider End -->
</section>
<section class="stopper spacing spacing-top mobile">
  <div class="grid start">
        <?php
      $args = array(
        'post_type' => 'galleries',
        'posts_per_page' => -1
      );
      // The Query
      $the_query = new WP_Query( $args );

      // The Loop
      if ( $the_query->have_posts() ) {
        while ( $the_query->have_posts() ) {
          $the_query->the_post(); ?>
              <!-- Module Start -->
              <div class="col-25">
                <a href="<?php echo get_permalink(); ?>">
                  <figure>
                    <?php
                      $rows = get_field('slider' ); // get all the rows
                      $first_row = $rows[0]; // get the first row
                      $first_row_image = $first_row['image' ]; // get the sub field value
                      $image = wp_get_attachment_image_src( $first_row_image, 'square-image' );
                    ?>
                    <img src="<?php echo $image[0]; ?>" alt="Gallery Picture">
                    <figcaption><?php the_title(); ?></figcaption>
                  </figure>
                </a>
              </div>
              <!-- Module End -->
      <?php }
      } else {
        echo '<h2>Da ist wohl was schief gelaufen, bitte kontaktieren Sie uns, damit wir schauen koennen, woran das liegt</h2>';
      }
      /* Restore original Post Data */
      wp_reset_postdata(); ?>

  </div>
</section>

<?php
include("parts/footer.php");
include("parts/html-footer.php");
?>
