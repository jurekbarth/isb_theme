<?php
/*
Template Name: Products
 */
include("parts/html-header.php");
?>
<body class="products">
<?php include("parts/header.php"); ?>
<section class="spacing-top">
  <div class="grid stopper spacing">
    <aside class="col-20">
      <?php
        $defaults = array(
          'theme_location'  => 'products',
          'menu'            => '',
          'container'       => '',
          'container_class' => '',
          'container_id'    => '',
          'menu_class'      => 'menu',
          'menu_id'         => '',
          'echo'            => true,
          'fallback_cb'     => '',
          'before'          => '',
          'after'           => '',
          'link_before'     => '',
          'link_after'      => '',
          'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
          'depth'           => 0,
          'walker'          => ''
        );
        wp_nav_menu( $defaults ); ?>
    </aside>
    <div class="col-80">
      <div class="grid start spacing">
      <?php
        $field = get_field('products');
        $args = array(
          'post_type' => 'products',
          'posts_per_page' => -1,
          'tax_query' => array(
            array(
              'taxonomy' => 'p-category',
              'field' => 'id',
              'terms' => $field
            )
          )
        );
        // The Query
        $the_query = new WP_Query( $args );

        // The Loop
        if ( $the_query->have_posts() ) {
          while ( $the_query->have_posts() ) {
            $the_query->the_post(); ?>
        <!-- product start -->
        <div class="product col-33">
          <a href="<?php echo get_permalink(); ?>">
          <?php
            $rows = get_field('slider' ); // get all the rows
            $first_row = $rows[0]; // get the first row
            $first_row_image = $first_row['image' ]; // get the sub field value
            $image = wp_get_attachment_image_src( $first_row_image, 'upright-image' );
          ?>
            <img src="<?php echo $image[0]; ?>" alt="Product Image">
            <?php if( get_field( "new" ) ): ?>
              <p class="new"><?php the_field("new"); ?></p>
            <?php endif; ?>
            <?php if( get_field( "sale" ) ): ?>
              <p class="offer"><?php the_field("sale"); ?></p>
            <?php endif; ?>
            <?php if( get_field( "producttitle" ) ): ?>
              <p class="info"><?php the_field("producttitle"); ?>
              <?php if( get_field( "price" ) ): ?>
                <span class="price"><?php the_field("price"); ?> €</span>
              <?php endif; ?>
            <?php endif; ?>
          </a>
        </div>
        <!-- product end -->
       <?php  }
        } else {
          echo '<h2>Ouch, something went wrong. Sorry :/. Let us now about this problem and contact us</h2>';
        }
        /* Restore original Post Data */
        wp_reset_postdata(); ?>
      </div>
    </div>
  </div>
</section>
<?php
include("parts/footer.php");
include("parts/html-footer.php");
?>
