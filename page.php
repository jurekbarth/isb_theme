<?php
/**
 * The template for displaying all pages.
 */
include("parts/html-header.php");
?>
<body>
<?php include("parts/header.php"); ?>
<section>
  <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
    <article class="stopper spacing small">
      <div class="center">
        <h1><?php the_title(); ?> <span class="border"></span></h1>
      </div>
      <?php the_content(); ?>
    </article>
  <?php endwhile; ?>
</section>

<?php
include("parts/footer.php");
include("parts/html-footer.php");
?>
