<?php
/*
Template Name: Events
 */
include("parts/html-header.php");
?>
<body class="events">
<?php include("parts/header.php"); ?>
<section>
  <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
  <div class="stopper spacing">
    <div class="center">
      <h1><?php the_title(); ?><span class="border"></span></h1>
    </div>
    <div class="events-wrapper">
    <?php if(have_rows('events')): ?>
      <ul class="slides">
        <?php while(have_rows('events')): the_row(); ?>
          <li>
            <h3><?php the_sub_field("headline"); ?></h3>
            <?php
              $attachment_id = get_sub_field('image');
              $size = "medium"; // (thumbnail, medium, large, full or custom size)
              $image = wp_get_attachment_image_src( $attachment_id, $size);
            ?>
            <img src="<?php echo $image[0]; ?>" alt="Gallery Image">
            <?php if( get_sub_field( "text" ) ): ?>
              <p><?php the_sub_field("text"); ?></p>
            <?php endif; ?>
            <?php if( get_sub_field( "link" ) ): ?>
              <a class="button" href="<?php the_sub_field("link"); ?>"><?php the_sub_field("link_text"); ?></a>
            <?php endif; ?>
            <?php if( get_sub_field( "date" ) ): ?>
              <p><?php the_sub_field("date"); ?></p>
            <?php endif; ?>
          </li>
        <?php endwhile; ?>
      </ul>
    <?php endif; ?>
    </div>
  </div>
  <?php endwhile; ?>
</section>

<?php
include("parts/footer.php");
include("parts/html-footer.php");
?>
