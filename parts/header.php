<header class="main-header cf">
  <div class="spacing stopper">
    <a href="<?php echo get_site_url(); ?>"><img class="logo" src="<?php echo get_template_directory_uri(); ?>/images/logo_png.png" alt="Isabella Leon"></a>
    <nav>
    <button class="mobile-menu-button">Menu</button>
    	<?php
				$defaults = array(
					'theme_location'  => 'main',
					'menu'            => '',
					'container'       => '',
					'container_class' => 'menu-wrapper',
					'container_id'    => '',
					'menu_class'      => 'main-menu',
					'menu_id'         => '',
					'echo'            => true,
					'fallback_cb'     => '',
					'before'          => '',
					'after'           => '',
					'link_before'     => '',
					'link_after'      => '',
					'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
					'depth'           => 0,
					'walker'          => ''
				);
				wp_nav_menu( $defaults );
			?>
    </nav>
  </div>
</header>
<div class="spacer"></div>
<a class="language" href="<?php the_field("en_de_link"); ?>" >Change Language de | en</a>




