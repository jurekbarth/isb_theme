<?php
  $field_id = get_field("footer_id", 5);
  the_field( "footer", $field_id );
?>

<a class="languagefooter" href="<?php the_field("en_de_link"); ?>" >Change Language de | en</a>
</footer>