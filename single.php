<?php
  include("parts/html-header.php");
?>
<body class="news-single">
  <?php include("parts/header.php"); ?>
  <section>
    <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
    <article class="stopper small">
      <div class="center">
        <h1><?php the_title(); ?></h1>
        <h5><time class="light" datetime="<?php the_time( 'Y-m-d' ); ?>"><?php the_date(); ?> </time></h5>
      </div>
    	<?php the_content(); ?>
    	<a class="button" href="/blog-news/">&larr; Go Back</a>
    </article>
    <?php endwhile; ?>
  </section>
<?php
  include("parts/footer.php");
  include("parts/html-footer.php");
?>