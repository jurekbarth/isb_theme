<?php
/**
 * The template for displaying 404 pages (Not Found)
 */
include("parts/html-header.php");
?>
<body class="standard">
<div class="page-wrap">
	<?php include("parts/header.php"); ?>
	<section class="main-content">
		<article>
			<h1>Seite wurde nicht gefunden.</h1>
		</article>
	</section>
</div>
<?php 
include("parts/html-footer.php"); 
?>