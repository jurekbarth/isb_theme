<?php
/**
 * The Template for displaying all single posts
 */
include("parts/html-header.php");
?>
<body class="products product-single">
<?php include("parts/header.php"); ?>

<section class="spacing-top">
  <div class="grid stopper spacing">
    <aside class="col-20">
      <?php
        $defaults = array(
          'theme_location'  => 'products',
          'menu'            => '',
          'container'       => '',
          'container_class' => '',
          'container_id'    => '',
          'menu_class'      => 'menu',
          'menu_id'         => '',
          'echo'            => true,
          'fallback_cb'     => '',
          'before'          => '',
          'after'           => '',
          'link_before'     => '',
          'link_after'      => '',
          'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
          'depth'           => 0,
          'walker'          => ''
        );
        wp_nav_menu( $defaults ); ?>
    </aside>
    <div class="col-80">
      <div class="grid around">
        <div class="col-50 spacing">
        <!-- flexslider start -->
        <?php if(have_rows('slider')): ?>
		      <div id="slider" class="flexslider flexslider-product">
            <ul class="slides">
			        <?php while(have_rows('slider')): the_row(); ?>
			          <li>
			              <?php
			                $attachment_id = get_sub_field('image');
			                $size = "product-image"; // (thumbnail, medium, large, full or custom size)
			                $image = wp_get_attachment_image_src( $attachment_id, $size);
			              ?>
			              <img src="<?php echo $image[0]; ?>" alt="Product Image">
			          </li>
			        <?php endwhile; ?>
		      	</ul>
          </div>
		    <?php endif; ?>
				<?php if(have_rows('slider')): ?>
		      <div id="carousel" class="flexslider flexslider-product carousel">
            <ul class="slides">
			        <?php while(have_rows('slider')): the_row(); ?>
			          <li>
			              <?php
			                $attachment_id = get_sub_field('image');
			                $size = "thumbnail-image"; // (thumbnail, medium, large, full or custom size)
			                $image = wp_get_attachment_image_src( $attachment_id, $size);
			              ?>
			              <img src="<?php echo $image[0]; ?>" alt="Product Image">
			          </li>
			        <?php endwhile; ?>
		      	</ul>
          </div>
		    <?php endif; ?>
        <!-- flexslider end -->
        </div>
        <div class="col-40">
          <h1><?php the_field("producttitle"); ?></h1>
          <?php if(have_rows('properties')): ?>
	          <ul>
	          	<?php while(have_rows('properties')): the_row(); ?>
	            	<li><span class="bold"><?php the_sub_field("property"); ?>:</span> <?php the_sub_field("value"); ?></li>
	            <?php endwhile; ?>
	          </ul>
	        <?php endif; ?>
          <?php if( get_field( "infotext" ) ): ?>
          	<p><?php the_field("infotext"); ?></p>
          <?php endif; ?>
          <?php if( get_field( "price" ) ): ?>
          	<h2 class="price"><?php the_field("price"); ?> €</h2>
          <?php endif; ?>
          <a href="<?php echo get_site_url(); ?>/shop/request-product" class="button"><?php the_field("request_product"); ?></a>
        </div>
      </div>
    </div>
  </div>
</section>
<?php
include("parts/html-footer.php");
?>