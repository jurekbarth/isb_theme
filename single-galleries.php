<?php
  include("parts/html-header.php");
?>
<body class="gallery">
<?php include("parts/header.php"); ?>
<section class="stopper spacing spacing-top">
 <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
    <!-- Flexslider Start -->
    <?php if(have_rows('slider')): ?>
      <div class="flexslider flexslider-standard">
        <ul class="slides">
          <?php while(have_rows('slider')): the_row(); ?>
            <li>
                <?php
                  $attachment_id = get_sub_field('image');
                  $size = "slider-normal-image"; // (thumbnail, medium, large, full or custom size)
                  $image = wp_get_attachment_image_src( $attachment_id, $size);
                ?>
                <img src="<?php echo $image[0]; ?>" alt="Gallery Image">
                <?php if( get_sub_field( "text" ) ): ?>
                  <div class="infotext">
                    <p><?php the_sub_field("text"); ?></p>
                  </div>
                <?php endif; ?>
            </li>
          <?php endwhile; ?>
        </ul>
      </div>
    <?php endif; ?>
    <!-- Flexslider End -->
    <article class="stopper-50">
      <?php the_content(); ?>
      <a class="button" href="/gallery/">← Zurück</a>
    </article>
    <?php endwhile; ?>
</section>
<?php
  include("parts/footer.php");
  include("parts/html-footer.php");
?>