<?php
/*
Template Name: Start
 */
include("parts/html-header.php");
?>
<body class="home">
<?php include("parts/header.php"); ?>
<section>
  <!-- Flexslider Start -->
  <?php if(have_rows('slider')): ?>
  <div class="flexslider flexslider-standard">
    <ul class="slides">
      <?php while(have_rows('slider')): the_row(); ?>
      <li class="limiter">
          <?php
            $attachment_id = get_sub_field('image');
            $size = "slider-big-image"; // (thumbnail, medium, large, full or custom size)
            $image = wp_get_attachment_image_src( $attachment_id, $size);
          ?>
  	      <img src="<?php echo $image[0]; ?>" alt="Slider Image" />
          <?php if( get_sub_field( "headline" ) ): ?>
    	      <div class="inpicture-infotext">
    	      	<h1><?php the_sub_field("headline"); ?></h1>
              <?php if( get_sub_field( "subheadline" ) ): ?>
    	      	  <h2><?php the_sub_field("subheadline"); ?></h2>
              <?php endif; ?>
              <?php if( get_sub_field( "link" ) ): ?>
                <div><a class="button" href="<?php the_sub_field("link"); ?>"><?php the_sub_field("link_text"); ?></a></div>
              <?php endif; ?>
    	      </div>
          <?php endif; ?>
      </li>
      <?php endwhile; ?>
    </ul>
  </div>
  <?php endif; ?>
  <!-- Flexslider End -->
</section>
<section class="center spacing stopper">
  <h1><?php the_field("headline_welcome"); ?> <span class="border"></span></h1>
  <p><?php the_field("text_welcome"); ?></p>
</section>
<section class="center spacing wwd">
  <div class="stopper">
    <h1><?php the_field("wwd_headline"); ?> <span class="border"></span></h1>
    <?php if(have_rows('wwd_topics')): ?>
      <div class="grid start">
        <?php while(have_rows('wwd_topics')): the_row(); ?>
          <article class="col-25">
            <a href="<?php the_sub_field("link"); ?>">
              <?php
                $attachment_id = get_sub_field('image');
                $size = "square-image"; // (thumbnail, medium, large, full or custom size)
                $image = wp_get_attachment_image_src( $attachment_id, $size);
              ?>
              <img src="<?php echo $image[0]; ?>" alt="Topic Image">
              <h3><?php the_sub_field("headline"); ?></h3>
              <p><?php the_sub_field("text"); ?></p>
            </a>
          </article>
        <?php endwhile; ?>
      </div>
    <?php endif; ?>
  </div>
</section>
<!-- <section class="center spacing latest-blog">
  <article class="stopper">
    <h1>Latest Blog <span class="border"></span></h1>
    <h2>Tango Around the World</h2>
    <h5>Posted on 14 Mar 2014 in News, Tango & me</h5>
    <p>Heute vor fünf Jahren habe ich den ersten Beitrag in diesem Blog veröffentlicht. Weil mir gerade nichts einfällt (ich sitze in der Arbeit) und weil ich sowieso finde, dass meine Kommentatorinnen und Kommentatoren viel besser schreiben als ich, lehne ich mich heute gemütlich zurück und warte, dass meine Leserinnen und Leser etwas schreiben...</p>
  </article>
</section> -->
<section class="center findus">
  <h1><?php the_field("fu_headline"); ?> <span class="border"></span></h1>
  <?php the_field("fu_map"); ?>
</section>

<?php
include("parts/footer.php");
include("parts/html-footer.php");
?>
