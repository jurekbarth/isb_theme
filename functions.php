<?php

// Add theme support post thumbnails
	add_theme_support( 'post-thumbnails', array( 'post' ) );

// Register Menu
	register_nav_menu('main', 'Main-Menu for the Site');
	register_nav_menu('products', 'Product Menu for the Site');

/**
 * Register `Products` post type
 */
function products_post_type() {

   // Labels
	$singular = 'Product';
	$plural = 'Products';
	$labels = array(
		'name' => _x($plural, "post type general name"),
		'singular_name' => _x($singular, "post type singular name"),
		'menu_name' => $plural,
		'add_new' => _x("Add $singular", "$singular"),
		'add_new_item' => __("Add $singular"),
		'edit_item' => __("Edit $singular"),
		'new_item' => __("New $singular"),
		'view_item' => __("View $singular"),
		'search_items' => __("Search $plural"),
		'not_found' =>  __("No $plural found"),
		'not_found_in_trash' => __("No $plural found in Trash"),
		'parent_item_colon' => ''
	);

	// Register post type
	register_post_type('products' , array(
		'labels' => $labels,
		'public' => true,
		'has_archive' => false,
		'rewrite' => true,
		'supports' => array('title', 'editor', 'excerpt', 'custom-fields', 'thumbnail')
	) );
}
add_action( 'init', 'products_post_type', 0 );

/**
 * Register `Products` taxonomy
 */
function products_taxonomy() {

	// Labels
	$singular = 'P-Category';
	$plural = 'P-Categories';
	$labels = array(
		'name' => _x( $plural, "taxonomy general name"),
		'singular_name' => _($singular),
		'search_items' =>  __("Search $singular"),
		'all_items' => __("All $plural"),
		'parent_item' => __("Parent $singular"),
		'parent_item_colon' => __("Parent $singular:"),
		'edit_item' => __("Edit $singular"),
		'update_item' => __("Update $singular"),
		'add_new_item' => __("Add $singular"),
		'new_item_name' => __("Add $singular Name"),
	);

	// Register and attach to 'Products' post type
	register_taxonomy( strtolower($singular), 'products', array(
		'public' => true,
		'show_ui' => true,
		'show_in_nav_menus' => true,
		'hierarchical' => true,
		'query_var' => true,
		'rewrite' => true,
		'labels' => $labels
	) );
}
add_action( 'init', 'products_taxonomy', 0 );

/**
 * Register `Gallery` post type
 */
function galleries_post_type() {

   // Labels
	$singular = 'Gallery';
	$plural = 'Galleries';
	$labels = array(
		'name' => _x($plural, "post type general name"),
		'singular_name' => _x($singular, "post type singular name"),
		'menu_name' => $plural,
		'add_new' => _x("Add $singular", "$singular"),
		'add_new_item' => __("Add $singular"),
		'edit_item' => __("Edit $singular"),
		'new_item' => __("New $singular"),
		'view_item' => __("View $singular"),
		'search_items' => __("Search $plural"),
		'not_found' =>  __("No $plural found"),
		'not_found_in_trash' => __("No $plural found in Trash"),
		'parent_item_colon' => ''
	);

	// Register post type
	register_post_type('galleries' , array(
		'labels' => $labels,
		'public' => true,
		'has_archive' => false,
		'rewrite' => true,
		'supports' => array('title', 'editor', 'excerpt', 'custom-fields', 'thumbnail')
	) );
}
add_action( 'init', 'galleries_post_type', 0 );

/**
 * Register `Products` taxonomy
 */
function galleries_taxonomy() {

	// Labels
	$singular = 'G-Category';
	$plural = 'G-Categories';
	$labels = array(
		'name' => _x( $plural, "taxonomy general name"),
		'singular_name' => _($singular),
		'search_items' =>  __("Search $singular"),
		'all_items' => __("All $plural"),
		'parent_item' => __("Parent $singular"),
		'parent_item_colon' => __("Parent $singular:"),
		'edit_item' => __("Edit $singular"),
		'update_item' => __("Update $singular"),
		'add_new_item' => __("Add $singular"),
		'new_item_name' => __("Add $singular Name"),
	);

	// Register and attach to 'Products' post type
	register_taxonomy( strtolower($singular), 'galleries', array(
		'public' => true,
		'show_ui' => true,
		'show_in_nav_menus' => true,
		'hierarchical' => true,
		'query_var' => true,
		'rewrite' => true,
		'labels' => $labels
	) );
}
add_action( 'init', 'galleries_taxonomy', 0 );



// Add custom Picture Sizes

if ( function_exists( 'add_image_size' ) ) {
	add_image_size( 'slider-big-image', 1200, 650, true ); //(cropped)
	add_image_size( 'slider-normal-image', 800, 600, false );
	add_image_size( 'square-image', 300, 300, true ); //(cropped)
	add_image_size( 'upright-image', 300, 400, true ); //(cropped)
	add_image_size( 'product-image', 500, 650, false );
	add_image_size( 'thumbnail-image', 120, 120, true ); //(cropped)
}



