<?php
/*
Template Name: Blog
 */
include("parts/html-header.php");
?>
<body class="blog">
<?php include("parts/header.php"); ?>
<section>
  <div class="stopper spacing">
    <div class="center">
      <h1>News / Blog<span class="border"></span></h1>
    </div>
      <?php
        $args = array(
          'post_type' => 'post',
          'posts_per_page' => -1
        );
        // The Query
        $the_query = new WP_Query( $args );

        // The Loop
        if ( $the_query->have_posts() ) {
          while ( $the_query->have_posts() ) {
            $the_query->the_post(); ?>
                <!-- Start -->
        <article class="grid">
          <div class="col-40">
            <h1><?php the_title(); ?></h1>
            <h5><time class="light" datetime="<?php the_time( 'Y-m-d' ); ?>"><?php the_date(); ?> </time></h5>
          </div>
          <div class="col-60">
            <p><?php the_excerpt(); ?></p>
            <a href="<?php echo get_permalink(); ?>" class="button">Read more</a>
          </div>
        </article>
                <!-- End -->
        <?php }
        } else {
          echo '<h2>Da ist wohl was schief gelaufen, bitte kontaktieren Sie uns, damit wir schauen koennen, woran das liegt</h2>';
        }
        /* Restore original Post Data */
        wp_reset_postdata();
      ?>



  </div>
</section>

<?php
include("parts/footer.php");
include("parts/html-footer.php");
?>
