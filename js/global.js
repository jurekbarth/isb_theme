// @codekit-prepend "libs/jquery-1.11.0.js"
// @codekit-prepend "libs/modernizr.custom.js"
// @codekit-prepend "libs/jquery.flexslider.js"

// Can also be used with $(document).ready()
$(window).load(function() {
  $('.flexslider-standard').flexslider({
    animation: "slide"
  });
});

// For Product Slider
$(window).load(function() {
  // The slider being synced must be initialized first
  $('#carousel').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 160,
    itemMargin: 0,
    asNavFor: '#slider'
  });

  $('#slider').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    sync: "#carousel"
  });
});

$('.mobile-menu-button').click(function(e) {
  e.preventDefault();
  $('.main-menu').toggleClass('active');
});


// Menu Fade in
var $document = $(window),
    $element = $('.main-header'),
    //$scrollheight = 525,
    className = 'smallheader';

if ($("body").hasClass("home")) {
  $scrollheight = 525;
} else {
  $scrollheight = 100;
}

$document.scroll(function() {
  $element.toggleClass(className, $document.scrollTop() >= $scrollheight);
});